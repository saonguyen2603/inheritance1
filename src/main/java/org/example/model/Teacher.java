package org.example.model;

public class Teacher extends Person {
    private int Expyear;

    public Teacher() {
        super();
    }

    public int getExpyear() {
        return Expyear;
    }

    public void setExpyear(int expyear) {
        Expyear = expyear;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public int getAge() {
        return super.getAge();
    }

    @Override
    public void setAge(int age) {
        super.setAge(age);
    }

    @Override
    public void input() {
        super.input();
        System.out.print("3. Số năm giảng dạy: ");
        int expYear = Integer.parseInt(sc.nextLine());
        setExpyear(expYear);
    }

    @Override
    public void displayInfo() {
        System.out.println("{Tên: " + super.getName() + "; Tuổi: " + super.getAge() + "; Năm kinh nghiệm: " + getExpyear() + "}");
    }
}
