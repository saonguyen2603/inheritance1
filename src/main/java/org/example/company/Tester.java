package org.example.company;

import org.example.model.Person;

import java.util.Scanner;

public class Tester extends Person {
    public static Scanner sc = new Scanner(System.in);
    private String id;
    private String testTool;

    public Tester() {
        super();
    }

    public String getTestTool() {
        return testTool;
    }

    public void setTestTool(String testTool) {
        this.testTool = testTool;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public int getAge() {
        return super.getAge();
    }

    @Override
    public void setAge(int age) {
        super.setAge(age);
    }

    @Override
    public void input() {
        super.input();
        System.out.print("3. Testool: "); String testTool = sc.nextLine();
        System.out.print("4. Id: "); String id = sc.nextLine();
        setTestTool(testTool);
        setId(id);
    }

    @Override
    public void displayInfo() {
        System.out.println("{Id: " +getId() + "; Tên: " + getName() + "; Tuổi: " + getAge() +"; TestTool: " + getTestTool() + "}");
    }
}
